# system modules
import unittest

# internal modules
from sdf import ease


class EasingTest(unittest.TestCase):
    def test_min_equals_max(self):
        for e in (ease.linear, ease.in_elastic, ease.in_out_cubic.symmetric):
            with self.subTest(e=e):
                self.assertEqual(e.min.value, -(-e).max.value)
                self.assertEqual(e.min.pos, (-e).max.pos)

    def test_is_ascending(self):
        self.assertTrue(ease.linear.is_ascending)
        self.assertTrue(ease.smoothstep.is_ascending)
        self.assertFalse((-ease.smoothstep).is_ascending)
        self.assertFalse(ease.in_bounce.is_ascending)

    def test_is_symmetric(self):
        self.assertFalse(ease.linear.is_symmetric)
        self.assertTrue(ease.linear.symmetric.is_symmetric)
        self.assertFalse(ease.out_bounce.is_symmetric)
        self.assertTrue(ease.out_bounce.symmetric.is_symmetric)

    def test_ordering(self):
        self.assertEqual(ease.linear, ease.linear)
        self.assertEqual(ease.constant(5), ease.constant(5))
        self.assertNotEqual(ease.smoothstep, ease.linear)
        self.assertLess(ease.smoothstep - 1, ease.linear)
        self.assertGreater(ease.smoothstep + 1, ease.linear)
